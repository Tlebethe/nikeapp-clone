import { View , FlatList, Image, Pressable} from 'react-native'
import React from 'react'
import { productsSlice } from '../store/ProductSlice';
import { useDispatch, useSelector } from 'react-redux';


export default function ProductsScreen( {navigation}) {
  const products = useSelector((state) => state.products.products);
  const dispatch = useDispatch();
  return (
    <View>
        <FlatList
        data={products}
        renderItem= {({item})=>(
          <View style={{padding: 2, width: '50%'}}>
            <Pressable onPress={ ()=> {
              //update selected item
              dispatch(productsSlice.actions.setSelectedProduct(item.id));
              navigation.navigate('Product Details')
              }}>
            <Image 
              source={{uri : item.image,}}
              style={{width: '100%', aspectRatio: 1}}
            />
            </Pressable>
          </View>
        )}
        numColumns={2}
      />
    </View>
  )
}