import { StyleSheet, View, FlatList, Image, useWindowDimensions, Text, ScrollView, Pressable } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch, useSelector } from 'react-redux';
import { cartSlice } from "../store/CartSlice";

export default function ProductDetailScreen() {
  const dispatch = useDispatch();
  const product = useSelector((state) => state.products.selectedProduct);
    const{ width } = useWindowDimensions();
  const addToCart = ()=> {
    dispatch(cartSlice.actions.addCartItem({product: product}))
  };
    const closeButton = () => console.warn('close the screen');
  return (
    <View>
       {/* Image Carousel */}
       <ScrollView>
       <FlatList
        data={product.images}
        renderItem={({item})=>(
            <Image 
            source={{uri: item}}
            style={{width: width, aspectRatio:1}}
        />
        )}
        horizontal
        pagingEnabled
       />
     
        <View style={{padding: 20}}>
          {/* Title */}
            <Text style={styles.title}>{product.name}</Text>
          {/* Price */}
            <Text style={styles.price}>{product.price}</Text>
          {/* Description */}
            <Text style={styles.description}>{product.description}</Text>
        </View>
      </ScrollView>
      {/* Add to cart button */}
      <Pressable style={styles.button} onPress={addToCart}>
        <Text  style={styles.textButton}>Add to cart</Text>
      </Pressable>
      {/* Navigation icon */}
      <Pressable style={styles.icons} onPress={closeButton}>
        <Ionicons name="close" size={24} color='white' />
      </Pressable>
    </View>
  )
}

const styles = StyleSheet.create({
    title : {
        fontSize: 35,
        fontWeight: '500',
        marginVertical: 10
    },
    price: {
        fontWeight: '500',
        fontSize: 16,
    },
    description: {
        marginVertical: 10,
        fontSize: 18,
        fontWeight: '300',
        lineHeight: 30,

    },
    button : {
      backgroundColor: 'black',
      position: 'absolute',
      bottom: 30,
      width: '90%',
      alignItems: 'center',
      alignSelf: 'center',
      padding: 20,
      borderRadius: 20,
    },
    textButton : {
      color: 'white',
      fontSize: 20,
      fontWeight: 'bold',
    },
    icons : {
      position: 'absolute',
      top: 20,
      backgroundColor: 'grey',
      right: 25,
      borderRadius: 20,
      padding: 5
    }
    
})