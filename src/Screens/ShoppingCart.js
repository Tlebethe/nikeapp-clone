import { FlatList, StyleSheet, View, Text, Pressable } from 'react-native';
import CartListItem from '../components/CartListItem';
import { useSelector } from 'react-redux';
import { selectDeliveryPrice} from '../store/CartSlice'


const ShoppingCart = () => {
  const cartItems = useSelector((state) => state.cartSlice.items);
  
  return (
    <View style={styles.upperContainer}>
    <FlatList
      data={cartItems}
      renderItem={({ item }) =>
       <CartListItem cartItem={item}

      />
    }
    ListFooterComponent={() => (
        <View style={styles.totalsContainer}>
          <View style={styles.row}>
            <Text style={styles.text}>Subtotal</Text>
            <Text style={styles.text}>410,00 US$</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.text}>Delivery</Text>
            <Text style={styles.text}>{selectDeliveryPrice}US$</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.textBold}>Total</Text>
            <Text style={styles.textBold}>426,50 US$</Text>
          </View>
        </View>
      )}
    />
    <View style={styles.footer}>
        <Pressable style={styles.button}>
            <Text style={styles.buttonText}>Checkout</Text>
        </Pressable>
    </View>
    </View>
  );
};

const styles = StyleSheet.create({
  upperContainer: {
      flex: 1,
  },

    totalsContainer: {
        margin: 20,
        paddingTop: 10,
        borderColor: "gainsboro",
        borderTopWidth: 1,
      },
      row: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginVertical: 2,
      },
      text: {
        fontSize: 16,
        color: "gray",
      },
      textBold: {
        fontSize: 16,
        fontWeight: "500",
      },
      footer: {
        position: "absolute",
        bottom: 0,
        width: "100%",
        backgroundColor: "white",
        borderColor: "gainsboro",
        borderTopWidth: 1,
        padding: 20,
      },
      
      button: {
        width: "100%",
        backgroundColor: "black",
        alignSelf: "center",
        alignItems: "center",
        padding: 20,
        borderRadius: 100,
      },
      buttonText: {
        color: "white",
        fontWeight: "500",
        fontSize: 16,
      },
})

export default ShoppingCart