import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ProductsScreen from './Screens/ProductsScreen';
import ProductDetailsScreen from './Screens/ProductDetailScreen';
import ShoppingCart from './Screens/ShoppingCart';
import { Pressable, Text , StyleSheet } from 'react-native';
import { useSelector } from 'react-redux';
import { FontAwesome5 } from '@expo/vector-icons';
import { selectNumberOfItems } from './store/CartSlice';


const Stack = createNativeStackNavigator();
  
  const Navigation = () => {
    const numberOfItems = useSelector(selectNumberOfItems);

    console.log(numberOfItems)
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ contentStyle: styles.container }}>
        <Stack.Screen 
            name="Products" 
            component={ProductsScreen} 
            options={({ navigation })=>({
                headerRight: ()=> (
                    <Pressable 
                      onPress={()=>navigation.navigate('Cart')}
                      style={{flexDirection: 'row'}}
                    >
                         <FontAwesome5 name="shopping-cart" size={18} color="gray" />
                         <Text style={styles.cartItems}>{numberOfItems}</Text>
                    </Pressable>
                )
            })}
        />
        <Stack.Screen 
            name="Product Details"
            component={ProductDetailsScreen}x
            options={{presentation: 'modal'}}
         />
        <Stack.Screen name="Cart" component={ShoppingCart} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
    
})


export default Navigation;