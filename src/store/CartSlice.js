
import { createSlice, createSelector } from '@reduxjs/toolkit';

//selector allows us to derive values from state

const initialState = {
  items: [],
  deliveryPrice: 15,
  freeDeliveryFrom: 200,
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addCartItem: (state, action) => {
        const newProduct = action.payload.product;
        console.log('new id', newProduct.id)
        console.log('items->', state.items)
        const found = state.items.find((item)=> item.product.id === newProduct.id)
        if (found) {
          console.log('adding quantity')
          found.quantity+=1
        }
        else {
          state.items.push({ product: newProduct, quantity: 1 });
          state.itemCount++;
          console.log(`added ${newProduct.name}`)
        }
        //console.warn(found)
       
    },
    changeQuantity: (state, action) => {
        const {id, quantity } = action.payload
        const cartItem = state.items.find((item)=>item.product.id ===id)
        if(cartItem){
          cartItem.quantity+=quantity
        }
        if (cartItem.quantity <= 0){
          state.items=state.items.filter((item)=> item.product.id !== id)
        }
    },
  },
  
});

export const selectNumberOfItems = (state) => state.cartSlice.items.length;

export const selectSubtotal = (state) =>
  state.cart.items.reduce(
    (sum, item) => sum + item.product.price * item.quantity,
    0
  );

export const selectSelf = (state) => state.cart;

export const selectDeliveryPrice = createSelector(
  selectSelf,
  selectSubtotal,
  (state, subtotal) =>
    subtotal > state.freeDeliveryFrom ? 0 : state.deliveryPrice
);

export const selectTotal = createSelector(
  selectSubtotal,
  selectDeliveryPrice,
  (subtotal, delivery) => subtotal + delivery
);