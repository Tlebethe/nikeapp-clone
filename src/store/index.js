import { configureStore } from '@reduxjs/toolkit';
import { productsSlice } from './ProductSlice';
import { cartSlice } from './CartSlice';

export const store = configureStore({
  reducer: {
    products: productsSlice.reducer,
    cartSlice: cartSlice.reducer,
  },
});